<?php
// $Id: publications_features.features.workflow.inc,v 1.01 2011/05/11 19:09:50 raman2385 $

/**
 * @file
 * for the publications features module
 */

/**
 * Implementation of hook_workflow_defaults().
 */
function publications_features_workflow_defaults() {
 $defaults = array();
  $defaults[] = array(
    'name' => 'Publications Feature Workflow',
    'machine_name' => 'publications_feature_workflow',
    'tab_roles' => array(
      '0' => 'Publications(M)',
      '1' => 'Publications(P)',
    ),
    'options' => array(
      'comment_log_node' => 1,
      'comment_log_tab' => 1,
      'name_as_title' => 1,
    ),
    'states' => array(
      '0' => array(
        'state' => '(creation)',
        'weight' => '-50',
        'sysid' => '1',
        'status' => '1',
        'module' => 'publications_features',
        'ref' => '0',
      ),
      '1301993963' => array(
        'state' => 'draft',
        'weight' => '0',
        'sysid' => '0',
        'status' => '1',
        'module' => 'publications_features',
        'ref' => '1301993963',
      ),
      '1301993978' => array(
        'state' => 'Published',
        'weight' => '0',
        'sysid' => '0',
        'status' => '1',
        'module' => 'publications_features',
        'ref' => '1301993978',
      ),
      '1301993990' => array(
        'state' => 'Unpublished',
        'weight' => '0',
        'sysid' => '0',
        'status' => '1',
        'module' => 'publications_features',
        'ref' => '1301993990',
      ),
    ),
    'roles' => array(
      'author' => array(
        'name' => 'author',
        'transitions' => array(
          '0' => array(
            'from' => '0',
            'to' => '1301993963',
          ),
        ),
      ),
      'Publications(M)' => array(
        'name' => 'Publications(M)',
        'transitions' => array(
          '0' => array(
            'from' => '0',
            'to' => '1301993963',
          ),
          '1' => array(
            'from' => '1301993963',
            'to' => '1301993990',
          ),
          '2' => array(
            'from' => '1301993990',
            'to' => '1301993978',
          ),
        ),
      ),
      'Publications(P)' => array(
        'name' => 'Publications(P)',
        'transitions' => array(
          '0' => array(
            'from' => '0',
            'to' => '1301993963',
          ),
          '1' => array(
            'from' => '0',
            'to' => '1301993978',
          ),
          '2' => array(
            'from' => '1301993963',
            'to' => '1301993978',
          ),
          '3' => array(
            'from' => '1301993963',
            'to' => '1301993990',
          ),
          '4' => array(
            'from' => '1301993978',
            'to' => '1301993963',
          ),
          '5' => array(
            'from' => '1301993978',
            'to' => '1301993990',
          ),
          '6' => array(
            'from' => '1301993990',
            'to' => '1301993978',
          ),
        ),
      ),
    ),
  );
  return $defaults;

}

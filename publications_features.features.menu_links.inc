<?php

/**
 * Implementation of hook_menu_default_menu_links().
 */
function publications_features_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-orders:admin/store/coupons
  $menu_links['menu-orders:admin/store/coupons'] = array(
    'menu_name' => 'menu-orders',
    'link_path' => 'admin/store/coupons',
    'router_path' => 'admin/store/coupons',
    'link_title' => 'Manage Coupons',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-orders:admin/store/customers
  $menu_links['menu-orders:admin/store/customers'] = array(
    'menu_name' => 'menu-orders',
    'link_path' => 'admin/store/customers',
    'router_path' => 'admin/store/customers',
    'link_title' => 'View/Edit Customers',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-orders:admin/store/orders/create
  $menu_links['menu-orders:admin/store/orders/create'] = array(
    'menu_name' => 'menu-orders',
    'link_path' => 'admin/store/orders/create',
    'router_path' => 'admin/store/orders/create',
    'link_title' => 'Create Orders',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-orders:admin/store/orders/view
  $menu_links['menu-orders:admin/store/orders/view'] = array(
    'menu_name' => 'menu-orders',
    'link_path' => 'admin/store/orders/view',
    'router_path' => 'admin/store/orders/view',
    'link_title' => 'View/Edit Orders',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-orders:products
  $menu_links['menu-orders:products'] = array(
    'menu_name' => 'menu-orders',
    'link_path' => 'products',
    'router_path' => 'products',
    'link_title' => 'View/Edit Products',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-publications:admin/settings/excel
  $menu_links['menu-publications:admin/settings/excel'] = array(
    'menu_name' => 'menu-publications',
    'link_path' => 'admin/settings/excel',
    'router_path' => 'admin/settings/excel',
    'link_title' => 'Orders',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
  );
  // Exported menu link: menu-publications:csv/uc_customers
  $menu_links['menu-publications:csv/uc_customers'] = array(
    'menu_name' => 'menu-publications',
    'link_path' => 'csv/uc_customers',
    'router_path' => 'csv/uc_customers',
    'link_title' => 'Customers',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-publications:export/publications 
  $menu_links['menu-publications:export/publications '] = array(
    'menu_name' => 'menu-publications',
    'link_path' => 'export/publications ',
    'router_path' => 'export/publications',
    'link_title' => 'Publications',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Exported menu link: menu-reports:admin/reports/filefield_tracker
  $menu_links['menu-reports:admin/reports/filefield_tracker'] = array(
    'menu_name' => 'menu-reports',
    'link_path' => 'admin/reports/filefield_tracker',
    'router_path' => 'admin/reports/filefield_tracker',
    'link_title' => 'File download report',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-reports:admin/store/reports/stock
  $menu_links['menu-reports:admin/store/reports/stock'] = array(
    'menu_name' => 'menu-reports',
    'link_path' => 'admin/store/reports/stock',
    'router_path' => 'admin/store/reports/stock',
    'link_title' => 'Stock',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-reports:sales_by_country
  $menu_links['menu-reports:sales_by_country'] = array(
    'menu_name' => 'menu-reports',
    'link_path' => 'sales_by_country',
    'router_path' => 'sales_by_country',
    'link_title' => 'Sales Countrywise Report',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-reports:store/publications/reports
  $menu_links['menu-reports:store/publications/reports'] = array(
    'menu_name' => 'menu-reports',
    'link_path' => 'store/publications/reports',
    'router_path' => 'store/publications/reports',
    'link_title' => 'Complimentary/ Discounted Publications',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Complimentary/ Discounted Publications');
  t('Create Orders');
  t('Customers');
  t('File download report');
  t('Manage Coupons');
  t('Orders');
  t('Publications');
  t('Sales Countrywise Report');
  t('Stock');
  t('View/Edit Customers');
  t('View/Edit Orders');
  t('View/Edit Products');


  return $menu_links;
}

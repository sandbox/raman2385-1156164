<?php
// $Id: publications_features.features.taxonomy.inc,v 1.01 2011/05/11 19:09:50 raman2385 $

/**
 * @file
 * for the publications features module
 */

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function publications_features_taxonomy_default_vocabularies() {
  return array();
}

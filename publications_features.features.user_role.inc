<?php

/**
 * Implementation of hook_user_default_roles().
 */
function publications_features_user_default_roles() {
  $roles = array();

  // Exported role: Publications(M)
  $roles['Publications(M)'] = array(
    'name' => 'Publications(M)',
  );

  // Exported role: Publications(P)
  $roles['Publications(P)'] = array(
    'name' => 'Publications(P)',
  );

  // Exported role: Super Admin
  $roles['Super Admin'] = array(
    'name' => 'Super Admin',
  );

  return $roles;
}

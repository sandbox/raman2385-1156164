<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function publications_features_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer nodequeue
  $permissions['administer nodequeue'] = array(
    'name' => 'administer nodequeue',
    'roles' => array(
      '0' => 'Super Admin',
    ),
  );

  // Exported permission: administer product stock
  $permissions['administer product stock'] = array(
    'name' => 'administer product stock',
    'roles' => array(),
  );

  // Exported permission: administer store
  $permissions['administer store'] = array(
    'name' => 'administer store',
    'roles' => array(
      '0' => 'Super Admin',
    ),
  );

  // Exported permission: administer workflow
  $permissions['administer workflow'] = array(
    'name' => 'administer workflow',
    'roles' => array(
      '0' => 'Super Admin',
    ),
  );

  // Exported permission: create orders
  $permissions['create orders'] = array(
    'name' => 'create orders',
    'roles' => array(),
  );

  // Exported permission: create publication products
  $permissions['create publication products'] = array(
    'name' => 'create publication products',
    'roles' => array(
      '0' => 'Publications(M)',
      '1' => 'Publications(P)',
      '2' => 'Super Admin',
    ),
  );

  // Exported permission: create publication_page content
  $permissions['create publication_page content'] = array(
    'name' => 'create publication_page content',
    'roles' => array(
      '0' => 'Publications(M)',
      '1' => 'Publications(P)',
      '2' => 'Super Admin',
    ),
  );

  // Exported permission: delete all publication products
  $permissions['delete all publication products'] = array(
    'name' => 'delete all publication products',
    'roles' => array(
      '0' => 'Super Admin',
    ),
  );

  // Exported permission: delete orders
  $permissions['delete orders'] = array(
    'name' => 'delete orders',
    'roles' => array(),
  );

  // Exported permission: delete own publication products
  $permissions['delete own publication products'] = array(
    'name' => 'delete own publication products',
    'roles' => array(
      '0' => 'Super Admin',
    ),
  );

  // Exported permission: edit all publication products
  $permissions['edit all publication products'] = array(
    'name' => 'edit all publication products',
    'roles' => array(
      '0' => 'Super Admin',
    ),
  );

  // Exported permission: edit any publication_page content
  $permissions['edit any publication_page content'] = array(
    'name' => 'edit any publication_page content',
    'roles' => array(
      '0' => 'Publications(M)',
      '1' => 'Publications(P)',
      '2' => 'Super Admin',
    ),
  );

  // Exported permission: edit orders
  $permissions['edit orders'] = array(
    'name' => 'edit orders',
    'roles' => array(
      '0' => 'Publications(P)',
      '1' => 'Super Admin',
    ),
  );

  // Exported permission: edit own publication products
  $permissions['edit own publication products'] = array(
    'name' => 'edit own publication products',
    'roles' => array(
      '0' => 'Super Admin',
    ),
  );

  // Exported permission: edit own publication_page content
  $permissions['edit own publication_page content'] = array(
    'name' => 'edit own publication_page content',
    'roles' => array(
      '0' => 'Publications(M)',
      '1' => 'Publications(P)',
      '2' => 'Super Admin',
    ),
  );

  // Exported permission: manage store coupons
  $permissions['manage store coupons'] = array(
    'name' => 'manage store coupons',
    'roles' => array(),
  );

  // Exported permission: schedule workflow transitions
  $permissions['schedule workflow transitions'] = array(
    'name' => 'schedule workflow transitions',
    'roles' => array(
      '0' => 'Super Admin',
    ),
  );

  // Exported permission: view all orders
  $permissions['view all orders'] = array(
    'name' => 'view all orders',
    'roles' => array(
      '0' => 'Publications(P)',
      '1' => 'Super Admin',
    ),
  );

  // Exported permission: view filefield_tracker reports
  $permissions['view filefield_tracker reports'] = array(
    'name' => 'view filefield_tracker reports',
    'roles' => array(
      '0' => 'Super Admin',
    ),
  );

  // Exported permission: view reports
  $permissions['view reports'] = array(
    'name' => 'view reports',
    'roles' => array(
      '0' => 'Publications(P)',
      '1' => 'Super Admin',
    ),
  );

  // Exported permission: view store coupons
  $permissions['view store coupons'] = array(
    'name' => 'view store coupons',
    'roles' => array(),
  );

  return $permissions;
}

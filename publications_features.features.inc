<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function publications_features_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_imagecache_default_presets().
 */
function publications_features_imagecache_default_presets() {
  $items = array(
    '150x100_publication_body_image' => array(
      'presetname' => '150x100_publication_body_image',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '150',
            'height' => '100%',
            'upscale' => 1,
          ),
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_rules_defaults().
 */
function publications_features_rules_defaults() {
  return array(
    'rules' => array(
      'publications_features_1' => array(
        '#type' => 'rule',
        '#set' => 'event_node_view',
        '#label' => 'Show access denied to other publishers/managers to view content if the content is in draft/unpublished state - UPDATED',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'Publications',
          'publications_features' => 'publications_features',
        ),
        '#status' => 'default',
        '#conditions' => array(),
        '#actions' => array(
          '0' => array(
            '#type' => 'action',
            '#settings' => array(
              'code' => '//check if the logged in user has the permissions to edit the content or not - if the content is in draft/unpublished state... if not, //show access denied
		//check the workflow state first
		if(($node->_workflow == 2) || ($node->_workflow == 4)){
			// check the logged in user has the right to edit that node
			global $user;
			$string_any = "edit any $node->type content";
			$string_own = "edit own $node->type content";
			if(!user_access($string_any, $user, $reset = FALSE) || !user_access($string_own, $user, $reset = FALSE)){
				//send an access denied
				drupal_access_denied();
                                exit;
			}
			else{
				//return FALSE;
			}
		}
                else{
                        //return TRUE;
                }
',
              'vars' => array(
                '0' => 'node',
                '1' => 'user',
              ),
            ),
            '#name' => 'rules_action_custom_php',
            '#info' => array(
              'label' => 'Execute custom PHP code',
              'module' => 'PHP',
              'eval input' => array(
                '0' => 'code',
              ),
            ),
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
      'publications_features_2' => array(
        '#type' => 'rule',
        '#set' => 'event_node_view',
        '#label' => 'Show access denied to other publishers/managers to view content if the content is in draft/unpublished state - UPDATED',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'Publications',
          'publications_features' => 'publications_features',
        ),
        '#status' => 'default',
        '#conditions' => array(),
        '#actions' => array(
          '0' => array(
            '#type' => 'action',
            '#settings' => array(
              'code' => '//check if the logged in user has the permissions to edit the content or not - if the content is in draft/unpublished state... if not, //show access denied
		//check the workflow state first
		if(($node->_workflow == 2) || ($node->_workflow == 4)){
			// check the logged in user has the right to edit that node
			global $user;
			$string_any = "edit any $node->type content";
			$string_own = "edit own $node->type content";
			if(!user_access($string_any, $user, $reset = FALSE) || !user_access($string_own, $user, $reset = FALSE)){
				//send an access denied
				drupal_access_denied();
                                exit;
			}
			else{
				//return FALSE;
			}
		}
                else{
                        //return TRUE;
                }
',
              'vars' => array(
                '0' => 'node',
                '1' => 'user',
              ),
            ),
            '#name' => 'rules_action_custom_php',
            '#info' => array(
              'label' => 'Execute custom PHP code',
              'module' => 'PHP',
              'eval input' => array(
                '0' => 'code',
              ),
            ),
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
      'publications_features_3' => array(
        '#type' => 'rule',
        '#set' => 'event_node_view',
        '#label' => 'Show access denied to other publishers/managers to view content if the content is in draft/unpublished state - UPDATED',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'Publications',
          'publications_features' => 'publications_features',
        ),
        '#status' => 'default',
        '#conditions' => array(),
        '#actions' => array(
          '0' => array(
            '#type' => 'action',
            '#settings' => array(
              'code' => '//check if the logged in user has the permissions to edit the content or not - if the content is in draft/unpublished state... if not, //show access denied
		//check the workflow state first
		if(($node->_workflow == 2) || ($node->_workflow == 4)){
			// check the logged in user has the right to edit that node
			global $user;
			$string_any = "edit any $node->type content";
			$string_own = "edit own $node->type content";
			if(!user_access($string_any, $user, $reset = FALSE) || !user_access($string_own, $user, $reset = FALSE)){
				//send an access denied
				drupal_access_denied();
                                exit;
			}
			else{
				//return FALSE;
			}
		}
                else{
                        //return TRUE;
                }
',
              'vars' => array(
                '0' => 'node',
                '1' => 'user',
              ),
            ),
            '#name' => 'rules_action_custom_php',
            '#info' => array(
              'label' => 'Execute custom PHP code',
              'module' => 'PHP',
              'eval input' => array(
                '0' => 'code',
              ),
            ),
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
      'publications_features_4' => array(
        '#type' => 'rule',
        '#set' => 'event_node_view',
        '#label' => 'Show access denied to other publishers/managers to view content if the content is in draft/unpublished state - UPDATED',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'Publications',
          'publications_features' => 'publications_features',
        ),
        '#status' => 'default',
        '#conditions' => array(),
        '#actions' => array(
          '0' => array(
            '#type' => 'action',
            '#settings' => array(
              'code' => '//check if the logged in user has the permissions to edit the content or not - if the content is in draft/unpublished state... if not, //show access denied
		//check the workflow state first
		if(($node->_workflow == 2) || ($node->_workflow == 4)){
			// check the logged in user has the right to edit that node
			global $user;
			$string_any = "edit any $node->type content";
			$string_own = "edit own $node->type content";
			if(!user_access($string_any, $user, $reset = FALSE) || !user_access($string_own, $user, $reset = FALSE)){
				//send an access denied
				drupal_access_denied();
                                exit;
			}
			else{
				//return FALSE;
			}
		}
                else{
                        //return TRUE;
                }
',
              'vars' => array(
                '0' => 'node',
                '1' => 'user',
              ),
            ),
            '#name' => 'rules_action_custom_php',
            '#info' => array(
              'label' => 'Execute custom PHP code',
              'module' => 'PHP',
              'eval input' => array(
                '0' => 'code',
              ),
            ),
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
      'publications_features_5' => array(
        '#type' => 'rule',
        '#set' => 'event_node_view',
        '#label' => 'Show access denied to other publishers/managers to view content if the content is in draft/unpublished state - UPDATED',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'Publications',
          'publications_features' => 'publications_features',
        ),
        '#status' => 'default',
        '#conditions' => array(),
        '#actions' => array(
          '0' => array(
            '#type' => 'action',
            '#settings' => array(
              'code' => '//check if the logged in user has the permissions to edit the content or not - if the content is in draft/unpublished state... if not, //show access denied
		//check the workflow state first
		if(($node->_workflow == 2) || ($node->_workflow == 4)){
			// check the logged in user has the right to edit that node
			global $user;
			$string_any = "edit any $node->type content";
			$string_own = "edit own $node->type content";
			if(!user_access($string_any, $user, $reset = FALSE) || !user_access($string_own, $user, $reset = FALSE)){
				//send an access denied
				drupal_access_denied();
                                exit;
			}
			else{
				//return FALSE;
			}
		}
                else{
                        //return TRUE;
                }
',
              'vars' => array(
                '0' => 'node',
                '1' => 'user',
              ),
            ),
            '#name' => 'rules_action_custom_php',
            '#info' => array(
              'label' => 'Execute custom PHP code',
              'module' => 'PHP',
              'eval input' => array(
                '0' => 'code',
              ),
            ),
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
      'publications_features_6' => array(
        '#type' => 'rule',
        '#set' => 'event_node_view',
        '#label' => 'Show access denied to other publishers/managers to view content if the content is in draft/unpublished state - UPDATED',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'Publications',
          'publications_features' => 'publications_features',
        ),
        '#status' => 'default',
        '#conditions' => array(),
        '#actions' => array(
          '0' => array(
            '#type' => 'action',
            '#settings' => array(
              'code' => '//check if the logged in user has the permissions to edit the content or not - if the content is in draft/unpublished state... if not, //show access denied
		//check the workflow state first
		if(($node->_workflow == 2) || ($node->_workflow == 4)){
			// check the logged in user has the right to edit that node
			global $user;
			$string_any = "edit any $node->type content";
			$string_own = "edit own $node->type content";
			if(!user_access($string_any, $user, $reset = FALSE) || !user_access($string_own, $user, $reset = FALSE)){
				//send an access denied
				drupal_access_denied();
                                exit;
			}
			else{
				//return FALSE;
			}
		}
                else{
                        //return TRUE;
                }
',
              'vars' => array(
                '0' => 'node',
                '1' => 'user',
              ),
            ),
            '#name' => 'rules_action_custom_php',
            '#info' => array(
              'label' => 'Execute custom PHP code',
              'module' => 'PHP',
              'eval input' => array(
                '0' => 'code',
              ),
            ),
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
      'publications_features_7' => array(
        '#type' => 'rule',
        '#set' => 'event_node_view',
        '#label' => 'Show access denied to other publishers/managers to view content if the content is in draft/unpublished state - UPDATED',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'Publications',
          'publications_features' => 'publications_features',
        ),
        '#status' => 'default',
        '#conditions' => array(),
        '#actions' => array(
          '0' => array(
            '#type' => 'action',
            '#settings' => array(
              'code' => '//check if the logged in user has the permissions to edit the content or not - if the content is in draft/unpublished state... if not, //show access denied
		//check the workflow state first
		if(($node->_workflow == 2) || ($node->_workflow == 4)){
			// check the logged in user has the right to edit that node
			global $user;
			$string_any = "edit any $node->type content";
			$string_own = "edit own $node->type content";
			if(!user_access($string_any, $user, $reset = FALSE) || !user_access($string_own, $user, $reset = FALSE)){
				//send an access denied
				drupal_access_denied();
                                exit;
			}
			else{
				//return FALSE;
			}
		}
                else{
                        //return TRUE;
                }
',
              'vars' => array(
                '0' => 'node',
                '1' => 'user',
              ),
            ),
            '#name' => 'rules_action_custom_php',
            '#info' => array(
              'label' => 'Execute custom PHP code',
              'module' => 'PHP',
              'eval input' => array(
                '0' => 'code',
              ),
            ),
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
    ),
  );
}

/**
 * Implementation of hook_uc_product_default_classes().
 */
function publications_features_uc_product_default_classes() {
  $items = array(
    'publication' => array(
      'name' => t('Publication'),
      'module' => 'uc_product',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function publications_features_views_api() {
  return array(
    'api' => '2',
  );
}

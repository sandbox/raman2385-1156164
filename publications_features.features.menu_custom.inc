<?php

/**
 * Implementation of hook_menu_default_menu_custom().
 */
function publications_features_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-orders
  $menus['menu-orders'] = array(
    'menu_name' => 'menu-orders',
    'title' => 'Manage Store',
    'description' => '',
  );
  // Exported menu: menu-publications
  $menus['menu-publications'] = array(
    'menu_name' => 'menu-publications',
    'title' => 'Publication Exports',
    'description' => '',
  );
  // Exported menu: menu-reports
  $menus['menu-reports'] = array(
    'menu_name' => 'menu-reports',
    'title' => 'Publication Reports',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Manage Store');
  t('Publication Exports');
  t('Publication Reports');


  return $menus;
}
